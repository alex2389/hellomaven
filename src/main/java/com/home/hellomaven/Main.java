package com.home.hellomaven;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello Maven from murlo!");
        System.out.println("Another Hello!");
        System.out.println("=== INFO ABOUT USER ===");
        Person alex = new Person("Alex", 45);
		Person vova = new Person("Vova", 63);
        System.out.println(alex);
        System.out.println(vova);
    }
}
